#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

char* strategies[6] = {"move UP", "move Left", "move Right", "move Down",\
                       "stay", "pick"};

typedef struct {
  char dna[243];
  int point;
}Body;

typedef enum {
  Wall=0,
  None,
  Garbage
}Status;

typedef Status Board[10][10];

const int pow3[] = {1, 3, 3*3, 3*3*3, 3*3*3*3};
const int dx[] = {0, -1, 1, 0, 0};
const int dy[] = {1, 0, 0, -1, 0};

int get_type(int x, int y, const Board b) {
    if (x >= 0 && x < 10 && y >= 0 && y < 10)
      return b[x][y];
    return Wall;
}

// randomly generate a body
void init_body(Body* body) {
  for (int i = 0; i < 243; ++i)
    body->dna[i] = '0'+rand()%6;
  body->point = 0;
}

// generate "garbage" of given cnt on given board
void init_board(Board board, int cnt) {
  int c=0;
  for (int i = 0; i < 10; ++i)
    for (int j = 0; j < 10; ++j) 
      board[i][j] = c++ < cnt ? Garbage : None;
  for (int i = 99; i > 0; --i) {
    int e = rand()%i;
    Status s = *(((Status*)board)+i);
    *(((Status*)board)+i) = *(((Status*)board)+e);
    *(((Status*)board)+e) = s;
  }
}

// take a dna piece and calc it's point on given board
int update(const char* dna, int times, int garbage_cnt) {
  int point_total = 0;
  static Board board;
  for (int t = 0; t < times; ++t) {
    init_board(board, garbage_cnt);
    int x = 0, y = 0;
    for (int i = 0; i < 200; ++i) {
      int situation = 0;
      for (int j = 0; j < 5; ++j)
        situation += pow3[j]*get_type(x+dx[j], y+dy[j], board);
      int strategy = dna[situation]-'0';
      if (strategy == 5) {
        point_total += get_type(x, y, board) == Garbage ? 10 : -1;
        board[x][y] = None;
      }
      else {
        Status s = get_type(x+dx[strategy], y+dy[strategy], board);
        if (s == Wall)
          point_total += -5;
        else {
          x += dx[strategy];
          y += dy[strategy];
        }
      }
    }
  }
  return point_total / times;
}

int choice(int max) {
  return rand()%(max/2);
}

int mutate(char* dna, double rate) {
  int cnt = 0;
  while(1.0*rand()/RAND_MAX < rate) {
    dna[rand()%243] = rand()%6 + '0';
    ++cnt;
  }
  return cnt;
}

int cmp(const void* a, const void* b) {
  return ((Body*)b)->point - ((Body*)a)->point;
}

const int N = 200;
const int M = 1000;
const int T = 100;

int main() {
  srand(time(0));
  FILE *file = fopen("data", "w");
  Body* bodies = (Body*)malloc(sizeof(Body)*N);
  Body* new_bodies = (Body*)malloc(sizeof(Body)*N);
  for (int r = 0; r < 10; ++r) {
    double rate = r/10.0;
    // step1: init all bodies randomly
    for (int i = 0; i < N; ++i) {
      init_body(&bodies[i]);
    }
    // step2: iterate M times
    for (int i = 0; i < M; ++i) {
      for (int j = 0; j < N; ++j) {
        // update all points
        bodies[j].point = update(bodies[j].dna, T, 50);
      }
      // step3: mate(?)
      // the one who earns more points is more likely to pass its dna
      qsort(bodies, N, sizeof(Body), cmp);
      int baby_cnt = 0;
      while (baby_cnt+1 < N) {
        int f = choice(N), m = choice(N);
        int bp = rand()%243;
        for (int c = 0; c < 243; ++c) {
          if (c < bp) {
            new_bodies[baby_cnt].dna[c] = bodies[f].dna[c];
            new_bodies[baby_cnt+1].dna[c] = bodies[m].dna[c];
          }
          else {
            new_bodies[baby_cnt].dna[c] = bodies[m].dna[c];
            new_bodies[baby_cnt+1].dna[c] = bodies[f].dna[c];
          }
        }
        mutate(new_bodies[baby_cnt].dna, rate);
        mutate(new_bodies[baby_cnt+1].dna, rate);
        baby_cnt += 2;
      }
      fprintf(file, "%d ", bodies[0].point);
      if ((i+1) % 100 == 0)
        printf("rate: %d%%, round %d, point %d\n", r*10, i, bodies[0].point);
      Body* b = bodies;
      bodies = new_bodies;
      new_bodies = b;
    }
    for (int len = 0; len < 243; ++len)
      fputc(new_bodies[0].dna[len], file);
    fputc('\n', file);
  }
  
  fclose(file);
  free(bodies);
  free(new_bodies);
  return 0;
}
